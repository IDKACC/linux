#!/bin/bash
# Post-Bootstrapping setup for chris
# curl -sSL -X GET https://gitlab.com/cssl/ChrisSimmons/public/linux/raw/master/scripts/post-bootstrap.sh | bash

if [ "$USER" != "chris" ]; then
  echo "This script is only meant to be run by user 'chris'." 1>&2
  exit 1
fi

if [[ -z "$GITLAB_PAT" ]]; then
    echo "Must provide GITLAB_PAT (GitLab Personal Access Token) in environment" 1>&2
    echo "Create one at https://gitlab.com/profile/personal_access_tokens" 1>&2
    exit 1
fi

echo "Installing required apt packages ..."
sudo apt install git jq -y

echo "Creating your SSH key ..."
curl -sSL -X GET https://gitlab.com/cssl/NetChris/public/linux/raw/master/scripts/re-generate-ssh-key.sh | bash

echo "Creating your basic git profile ..."
curl -sSL -X GET https://gitlab.com/cssl/ChrisSimmons/public/linux/raw/master/scripts/git-configs.sh | bash

echo "Sending your SSH public key to GitLab ..."
curl -sSL -X GET https://gitlab.com/cssl/NetChris/public/linux/raw/master/scripts/send-ssh-key-to-gitlab.sh | bash

echo "Cloning your User configuration and scripts ..."
git clone git@gitlab.com:cssl/ChrisSimmons/linux/home.git ~/.netchris

echo "Configuring your environment ..."
~/.netchris/configure.sh

echo "Done!"
echo "Now logout / login and ensure no password prompt"

#!/bin/sh
# Basic git configuration
# Primarily used to bootstrap the private linux configuration repository for Chris

git config --global user.email "chris.dev@netchris.com"
git config --global user.name "Chris Simmons"
